#include "mysh.h"

/*
 CITS2002 Project 2 2015
 Name(s):             Eric Goodwin
 Student number(s):   21488108
 Date:                date-of-submission
 */

//  THREE INTERNAL VARIABLES (SEE mysh.h FOR EXPLANATION)
char *HOME, *PATH, *CDPATH, *PATHS_LIST, *CURRENT_PATH;

char *argv0 = NULL; // the program's name
bool interactive = false;

char BIN_PATHS[MAX_PATHS][MAX_PATH_LEN];
int BIN_PATHS_COUNT = 0;

char CD_PATHS_ARRAY[MAX_PATHS][MAX_PATH_LEN];
int CD_PATHS_COUNT = 0;

char INTERNAL_CMDS[INTERNAL_CMDS_COUNT][MAX_INTERNAL_CMD_LEN] = {"exit", "cd", "set"};

int last_exit_status = 0; // exit status of last executed cmd, defaults to 0.

// ------------------------------------------------------------------------

void check_allocation0(void *p, char *file, const char *func, int line) {
	if (p == NULL) {
		fprintf(stderr, "%s, %s() line %i: unable to allocate memory\n", file,
				func, line);
		exit(2);
	}
}

static void print_redirection(CMDTREE *t) {
	if (t->infile != NULL)
		printf("< %s ", t->infile);
	if (t->outfile != NULL) {
		if (t->append == false)
			printf(">");
		else
			printf(">>");
		printf(" %s ", t->outfile);
	}
}

void print_cmdtree0(CMDTREE *t) {
	if (t == NULL) {
		printf("<nullcmd> ");
		return;
	}

	switch (t->type) {
	case N_COMMAND:
		for (int a = 0; a < t->argc; a++)
			printf("%s ", t->argv[a]);
		print_redirection(t);
		break;

	case N_SUBSHELL:
		printf("( ");
		print_cmdtree0(t->left);
		printf(") ");
		print_redirection(t);
		break;

	case N_AND:
		print_cmdtree0(t->left);
		printf("&& ");
		print_cmdtree0(t->right);
		break;

	case N_OR:
		print_cmdtree0(t->left);
		printf("|| ");
		print_cmdtree0(t->right);
		break;

	case N_PIPE:
		print_cmdtree0(t->left);
		printf("| ");
		print_cmdtree0(t->right);
		break;

	case N_SEMICOLON:
		print_cmdtree0(t->left);
		printf("; ");
		print_cmdtree0(t->right);
		break;

	case N_BACKGROUND:
		print_cmdtree0(t->left);
		printf("& ");
		print_cmdtree0(t->right);
		break;

	default:
		fprintf(stderr, "%s: invalid NODETYPE in print_cmdtree0()\n", argv0);
		exit(1);
		break;
	}
}

/**
 * Break a string up by the given delimiter.
 *
 * @param char delimiter
 *   The character to explode the string on.
 * @param char input_string[]
 *   Input string to parse.
 * @param char output_string[][]
 *   Output array to store results.
 *
 * @return integer
 *   The number of results.
 */
int explode_string(char delimiter, char input_string[], char output_string[][MAX_PATH_LEN]) {
	int i = 0, j = 0, result_count = 0;

	while (input_string[i] != '\0' && i < MAX_PATH_LEN && result_count < MAX_EXPLODE_RESULTS) {
		if (input_string[i] != delimiter) {
			// Add this character to the current output result and increment the char count (i).
			output_string[result_count][j] = input_string[i];
			i++;
			j++;
		} else {
			// This character if the delimiter, so skip it and start a new result.
			i++;
			j = 0;
			result_count++;
		}
	}

	// This count should start from 1.
	return result_count+1;
}

/**
 * Check if a file exists and is readable.
 *
 * @param char fname
 *   Filename to attempt to open.
 *
 * @result integer
 *   1 if exists and is readable, 0 otherwise.
 */
int file_exists(const char *fname) {
    FILE *file;

    // Attempt to open file for reading...
    if ((file = fopen(fname, "r"))) {
        fclose(file);
        return 1;
    }

    return 0;
}

/**
 * Check if a directory exists.
 *
 * @param char dir_path
 *   directory path to attempt to open.
 *
 * @result integer
 *   1 if exists 0 otherwise.
 */
int dir_exists(const char *dir_path) {
	DIR *dir;
	dir = opendir(dir_path);

	if (dir != NULL) {
		closedir(dir);
		return 1;
	}

	return 0;
}

/**
 * Check if a command is an internal command.
 *
 * @param char cmd
 *   Command to check.
 *
 * @return integer
 *   1 if it is a reserved internal cmd, 0 otherwise.
 */
int is_internal_command(const char *cmd) {
	int i = 0;

	while (i < INTERNAL_CMDS_COUNT) {
		if (strcmp(cmd, INTERNAL_CMDS[i]) == 0) {
			return 1;
		}
		i++;
	}

	return 0;
}

/**
 * Replace all (or a maximum) number of occurrences of a given string.
 *
 * @param string search
 *   The value being searched for (needle).
 * @param string replace
 *   The string to replace the needle with.
 * @param string subject
 *   The subject (haystack) to search.
 *
 * @return char
 *   A string with the replacements made.
 */
char *str_replace(char *search, char *replace, char *subject) {
	int search_len = strlen(search);

	// Sanity check. Search can't be larger than subject.
	if (search_len > strlen(subject)) {
		fprintf(stderr, "str_replace: Needle cannot be larger than haystack!");
		return subject;
	}

	char *j = NULL, *result = NULL, *old_subject = NULL;
	int rpl_count = 0;

	// Get the number of replacements to make.
	for (j = strstr(subject, search); j != NULL; j = strstr(j + search_len, search)) {
		rpl_count++;
	}

	// If there are no replacements needed, just return the subject.
	if (rpl_count == 0) {
		return subject;
	}

	// Get size of final string.
	rpl_count = (strlen(replace) - search_len) * rpl_count + strlen(subject);

	// Allocate some space for the result.
	result = malloc(rpl_count);
	strcpy(result, "");
	old_subject = subject;

	for (j = strstr(subject, search); j != NULL; j = strstr(j + search_len, search)) {
		strncpy(result + strlen(result), old_subject, j - old_subject);
		strcpy(result + strlen(result), replace);
		old_subject = j + search_len;
	}

	strcpy(result + strlen(result), old_subject);

	return result;
}
