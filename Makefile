C99     =  cc -std=c99
CFLAGS  =  -Wall -pedantic -Werror

PROJECT =  mysh
SOURCES = $(wildcard *.c)
HEADERS =  $(PROJECT).h
OBJECTS = $(SOURCES:.c=.o)

# Main target
$(PROJECT): $(OBJECTS)
	$(C99) $(OBJECTS) -o $(PROJECT)

# To obtain object files
%.o: %.c $(HEADERS)
	$(C99) -c $(CC_FLAGS) $< -o $@

# To remove generated files
clean:
	rm -f $(PROJECT) $(OBJECTS)
	