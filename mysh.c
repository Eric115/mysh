#include "mysh.h"

/*
 CITS2002 Project 2 2015
 Name(s):             Eric Goodwin
 Student number(s):   21488108
 Date:                date-of-submission
 */

int main(int argc, char *argv[]) {
//  REMEMBER THE PROGRAM'S NAME (TO REPORT ANY LATER ERROR MESSAGES)
	argv0 = (argv0 = strrchr(argv[0], '/')) ? argv0 + 1 : argv[0];
	argc--; // skip 1st command-line argument
	argv++;

//  INITIALIZE THE THREE INTERNAL VARIABLES
	char *p;

	p = getenv("HOME");
	HOME = strdup(p == NULL ? DEFAULT_HOME : p);
	check_allocation(HOME);

	p = getenv("PATH");
	PATH = strdup(p == NULL ? DEFAULT_PATH : p);
	check_allocation(PATH);

	p = getenv("CDPATH");
	CDPATH = strdup(p == NULL ? DEFAULT_CDPATH : p);
	check_allocation(CDPATH);

	set_path_arrays();

	// Start shell in home dir.
	chdir(HOME);

//  DETERMINE IF THIS SHELL IS INTERACTIVE
	interactive = (isatty(fileno(stdin)) && isatty(fileno(stdout)));

	int exitstatus = EXIT_SUCCESS;

//  READ AND EXECUTE COMMANDS FROM stdin UNTIL IT IS CLOSED (with control-D)
	while (!feof(stdin)) {
		CMDTREE *t = parse_cmdtree(stdin);

		if (t != NULL) {
			//print_cmdtree(t);
			exitstatus = execute_cmdtree(t);
			free_cmdtree(t);
		}
	}

	if (interactive) {
		fputc('\n', stdout);
	}

	return exitstatus;
}

/**
 * Set custom global path vars.
 */
void set_path_arrays() {
//	Explode PATH variable into array.
	BIN_PATHS_COUNT = explode_string(PATH_DELIM, PATH, BIN_PATHS);
// 	Explode CDPATH variable into array.
	CD_PATHS_COUNT = explode_string(PATH_DELIM, CDPATH, CD_PATHS_ARRAY);
}
