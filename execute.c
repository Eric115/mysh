#include "mysh.h"

/*
 CITS2002 Project 2 2015
 Name(s):		Eric Goodwin
 Student number(s):	21488108
 Date:		date-of-submission
 */

// -------------------------------------------------------------------
//  THIS FUNCTION SHOULD TRAVERSE THE COMMAND-TREE and EXECUTE THE COMMANDS
//  THAT IT HOLDS, RETURNING THE APPROPRIATE EXIT-STATUS.
//  READ print_cmdtree0() IN globals.c TO SEE HOW TO TRAVERSE THE COMMAND-TREE

#define MAXARGS 254
#define PATH_SLASH '/'

int execute_cmdtree(CMDTREE *t) {
	int exitstatus;

	if (t == NULL) {
		exitstatus = EXIT_FAILURE;
	} else {

		switch (t->type) {
			case N_COMMAND: {
				return t_cmd(t);
				break;
			}

			case N_SEMICOLON: {
				if (t->left != NULL) {
					execute_cmdtree(t->left);
				}

				if (t->right != NULL) {
					execute_cmdtree(t->right);
				}

				break;
			}

			case N_AND: {
				if (t->left != NULL) {
					execute_cmdtree(t->left);

					if (last_exit_status == 0) {
						if (t->right != NULL) {
							execute_cmdtree(t->right);
						}
					}
				}

				break;
			}

			case N_OR: {
				if (t->left != NULL) {
					execute_cmdtree(t->left);

					// only execute next if previous failed.
					if (last_exit_status != 0) {
						if (t->right != NULL) {
							execute_cmdtree(t->right);
						}
					}
				}

				break;
			}

			case N_SUBSHELL: {
				if (t->left != NULL) {
					execute_cmdtree(t->left);
				}

				if (t->right != NULL) {
					execute_cmdtree(t->right);
				}

				break;
			}

			case N_PIPE: {
				break;
			}

			case N_BACKGROUND: {
				if (t->left != NULL) {
					int pid;

					switch(pid = fork()) {
					// Error handling.
						case -1: {
							perror("fork()");
							fprintf(stderr, "Unable to fork process!");
							exit(1);
							break;
						}

						// Child process.
						case 0: {
							int child_status = execute_cmdtree(t->left);
							fprintf(stderr, "[%d] done.\n", getpid());
							exit(child_status);
							break;

						}

						// Parent.
						default: {
							if (t->right != NULL) {
								execute_cmdtree(t->right);
							}
							break;
						}
					}
				}

				break;
			}
		}
	}

	return exitstatus;
}

int t_cmd(CMDTREE *t) {
	if (!is_internal_command(t->argv[0])) {
		// First check if binary name is full path, no sense looking if we don't have to.
		if (strchr(t->argv[0], PATH_SLASH) != NULL) {
			run_bin(t->argv[0], t->argv, t->argc);
		} else {
			int i = 0, cmd_run = 0;
			// Iterate through possible bin paths and check for binary.
			while (i < BIN_PATHS_COUNT) {
				char tmp_path[MAX_PATH_LEN];

				strcpy(tmp_path, BIN_PATHS[i]);
				strcat(tmp_path, "/");
				strcat(tmp_path, t->argv[0]);

				if (file_exists(tmp_path)) {
					run_bin(tmp_path, t->argv, t->argc);
					cmd_run = 1;
					break;
				}

				i++;
			}

			if (!cmd_run) {
				fprintf(stderr, "%s: command not found: %s", argv0, t->argv[0]);
				last_exit_status = -1;
			}
		}
	} else {
		run_internal_cmd(t);
		last_exit_status = 0;
	}

	return last_exit_status;
}


/**
 * Run a binary with args.
 *
 * @param char path
 *   Path to the binary.
 * @param char argv
 *   Arguments.
 * @param integer argc
 *   Number of arguments in argv.
 */
int run_bin(const char *path, char **argv, int argc) {
	int pid, status;

	switch(pid = fork()) {
	// Error handling.
		case -1: {
			perror("fork()");
			fprintf(stderr, "Unable to fork process!");
			exit(1);
			break;
		}

		// Child process.
		case 0: {
			if (execv(path, argv) == -1) {
				perror("execv");
				fprintf(stderr, "%s", strerror(errno));
				exit(errno);
				break;
			}

			exit(0);
			break;

		}

		// Parent process.
		default: {
			while (wait(&status) > 0) {
				;
			}
			status = WEXITSTATUS(status);
			// Update last exit status.
			last_exit_status = status;
			break;
		}
	}

	return last_exit_status;
}

/**
 * Run an internal command.
 *
 * @param CMDTREE t
 *   The command tree. Must be of type N_COMMAND.
 */
void run_internal_cmd(CMDTREE *t) {
	if (t->type == N_COMMAND) {
		if (strcmp(t->argv[0], "exit") == 0) {
			// Check if exit has an argument to use as exit status.
			if (t->argv[1] != NULL) {
				int exit_status = (int) strtol(t->argv[1], NULL, 10);
				exit(exit_status);
			} else {
				// Exit with the last exit status (or default of 0).
				exit(last_exit_status);
			}
		} else if (strcmp(t->argv[0], "cd") == 0) {
			if (t->argv[1] == NULL) {
				// If no args, just go to the home dir.
				chdir(HOME);
			} else {
				if (strchr(t->argv[1], PATH_SLASH) != NULL) {
					char *go_to_path = str_replace("~", HOME, t->argv[1]);

					if (chdir(go_to_path) == -1) {
						fprintf(stderr, "Unable to change directory!");
					}
				} else {
					// Check dirs in CDPATH for the dir to change into.
					int i = 0, dir_found = 0;
					// Iterate through paths.
					while (i < CD_PATHS_COUNT) {
						char tmp_path[MAX_PATH_LEN];

						strcpy(tmp_path, CD_PATHS_ARRAY[i]);
						strcat(tmp_path, "/");
						strcat(tmp_path, t->argv[1]);

						if (dir_exists(tmp_path)) {
							if (chdir(tmp_path) == -1) {
								fprintf(stderr, "Unable to change directory!");
							}

							dir_found = 1;
							break;
						}

						i++;
					}

					if (!dir_found) {
						fprintf(stderr, "Unable to find directory!");
					}
				}
			}

		} else if (strcmp(t->argv[0], "time") == 0) {

		} else if (strcmp(t->argv[0], "set") == 0) {
			if (strcmp(t->argv[1],"HOME") == 0) {
				HOME = t->argv[2];
				set_path_arrays();
			}

			if (strcmp(t->argv[1], "PATH") == 0) {
				PATH = t->argv[2];
				set_path_arrays();
			}

			if (strcmp(t->argv[1], "CDPATH") == 0) {
				CDPATH = t->argv[2];
				set_path_arrays();
			}
		}

	} else {
		fprintf(stderr, "%s: Error: CMDTREE not of type N_COMMAND passed to run_internal_cmd!", argv0);
	}
}
