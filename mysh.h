#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <errno.h>
#include <dirent.h>

#if defined(__linux__)
    extern	char	*strdup(const char *str);
    extern	int	fileno(const FILE *fp);
#endif

//  Written by Chris.McDonald@uwa.edu.au, October 2015

// ----------------------------------------------------------------------

#define	DEFAULT_HOME	"/tmp"
#define	DEFAULT_PATH	"/bin:/usr/bin:/usr/local/bin:."
#define	DEFAULT_CDPATH	".:.."

#define COMMENT_CHAR	'#'	// comment character
#define HOME_CHAR	'~'	// home directory character

#define MAX_EXPLODE_RESULTS 100 // for explode_string function.
#define MAX_PATHS 10
#define MAX_PATH_LEN 256
#define PATH_DELIM ':'

#define MAX_INTERNAL_CMD_LEN 15
#define INTERNAL_CMDS_COUNT 3

//  ----------------------------------------------------------------------

//  AN enum IN C99 'GENERATES' A SEQUENCE OF UNIQUE, ASCENDING CONSTANTS
typedef enum {
	N_AND = 0,		// as in   cmd1 && cmd2
	N_BACKGROUND,		// as in   cmd1 &
	N_OR,			// as in   cmd1 || cmd2	
	N_SEMICOLON,		// as in   cmd1 ;  cmd2	
	N_PIPE,			// as in   cmd1 |  cmd2	
	N_SUBSHELL,		// as in   ( cmds )
	N_COMMAND		// an actual command node itself
} NODETYPE;


typedef	struct ct {
    NODETYPE	type;		// the type of the node, &&, ||, etc

    int		argc;		// the number of args iff type == N_COMMAND
    char	**argv;		// the NULL terminated argument vector

    char	*infile;	// as in    cmd <  infile
    char	*outfile;	// as in    cmd >  outfile
    bool	append;		// true iff cmd >> outfile

    struct ct	*left, *right;	// pointers to left and right subtrees
} CMDTREE;


extern CMDTREE	*parse_cmdtree(FILE *);		// in parser.c
extern void	free_cmdtree(CMDTREE *);	// in parser.c
extern int	execute_cmdtree(CMDTREE *);	// in execute.c


/* The global variable HOME points to a directory name stored as a
   character string. This directory name is used to indicate two things:

    - the directory used when the  cd  command is requested without arguments.
    - replacing the leading '~' character in args,  e.g.  ~/filename

   The HOME variable is initialized with the value inherited from the
   invoking environment (or DEFAULT_HOME if undefined).
 */

extern	char	*HOME;

/* The global variables PATH and CDPATH point to character strings representing
   colon separated lists of directory names.

   The value of PATH is used to search for executable files when a command
   name does not contain a '/'

   Similarly, CDPATH provides a colon separated list of directory names
   that are used in an attempt to change the current working directory.
 */

extern	char	*PATH;
extern	char	*CDPATH;
extern 	char	*CURRENT_PATH;

extern 	char 	CD_PATHS_ARRAY[MAX_PATHS][MAX_PATH_LEN];
extern 	int		CD_PATHS_COUNT;

extern	char	*argv0;		// The name of the shell, typically mysh
extern	bool	interactive;	// true if mysh is connected to a 'terminal'

extern 	char	BIN_PATHS[MAX_PATHS][MAX_PATH_LEN]; // PATH should only be split once.
extern  int		BIN_PATHS_COUNT;
extern	char	INTERNAL_CMDS[INTERNAL_CMDS_COUNT][MAX_INTERNAL_CMD_LEN];
extern	int		last_exit_status;


//  ----------------------------------------------------------------------

//  TWO PRE-PROCESSOR MACROS THAT MAY HELP WITH DEBUGGING YOUR CODE.
//      check_allocation(p) ENSURES THAT A POINTER IS NOT NULL, AND
//      print_cmdtree(t)  PRINTS THE REQUESTED COMMAND-TREE
//  THE TWO ACTUAL FUNCTIONS ARE DEFINED IN globals.c

#define	check_allocation(p)	\
	check_allocation0(p, __FILE__, __func__, __LINE__)
extern	void check_allocation0(void *p, char *file, const char *func, int line);

#define	print_cmdtree(t)	\
	printf("called from %s, %s() line %i:\n", __FILE__,__func__,__LINE__); \
	print_cmdtree0(t)
extern	void print_cmdtree0(CMDTREE *t);

//	Other functions
// Foreach functionality. (http://stackoverflow.com/questions/400951/does-c-have-a-foreach-loop-construct/400970#400970)
#define foreach(item, array) \
    for(int hold = 1, count = 0, size = sizeof (array) / sizeof *(array); hold && count != size; hold = !hold, count++) \
      for(item = (array) + count; hold; hold = !hold)

extern int run_bin(const char *path, char **arg_v, int argc);
extern int explode_string(char delimiter, char input_string[], char output_string_array[][MAX_PATH_LEN]);
extern char *str_replace(char *search, char *replace, char *subject);
extern int file_exists(const char *fname);
extern int is_internal_command(const char *cmd);
extern void run_internal_cmd(CMDTREE *t);
extern int dir_exists(const char *dir_name);
extern int t_cmd(CMDTREE *t);
extern void set_path_arrays();
